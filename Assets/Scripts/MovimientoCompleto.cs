using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MovimientoCompleto : MonoBehaviour
{
    public Camera Cam;
    Rigidbody rb;

    public float mouseHorizontal = 3f;
    public float mouseVertical = 2f;

    float h_mouse;
    float v_mouse;

    public float moveSpeed = 10f;
    public float runSpeed = 8;

    float h;
    float v;

    bool floorDetected = false;
    bool isJump = false;
    bool isRestart = false;
    public float jumpForce = 5.0f;

    public GameObject SaltoPJ;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        Move();
    }

    void Move()
    {
        h_mouse = mouseHorizontal * Input.GetAxis("Mouse X");
        v_mouse = mouseVertical * Input.GetAxis("Mouse Y");

        transform.Rotate(0, h_mouse, 0);
        Cam.transform.Rotate(-v_mouse, 0, 0);

        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");

        Vector3 direction = new Vector3(h, 0, v);
        transform.Translate(direction * moveSpeed * Time.deltaTime);

        Vector3 floor = transform.TransformDirection(Vector3.down);

        if (Physics.Raycast(transform.position, floor, 1.8f))
        {
            floorDetected = true;
            print("Hay contacto con el suelo");

        }
        else
        {
            floorDetected = false;
            print("No hay contacto con el suelo");
        }


        isJump = Input.GetButtonDown("Jump");

        if (isJump && floorDetected)
        {
            rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            Instantiate(SaltoPJ);
        }

        isRestart = Input.GetButtonDown("Restart");

        if (isRestart == true)
        {
            reiniciarNivel();
        }
    }

    public void reiniciarNivel()
    {
        SceneManager.LoadScene("Parcial");

    }


    public void masVelocidad(float amount)
    {
        moveSpeed += amount;
    }

    public void masTama�o(float amount)
    {
        transform.localScale += new Vector3(amount, amount, amount);
    }


}
